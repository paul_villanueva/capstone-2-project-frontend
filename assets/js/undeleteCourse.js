let params = new URLSearchParams(window.location.search);

let courseId = params.get('courseId');

let token = localStorage.getItem('token');

fetch(`https://pure-ravine-04682.herokuapp.com/api/courses/${courseId}`, {
	method: 'PUT',
    headers: {
    	'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
	},
	body: JSON.stringify({
		courseId: courseId
	})
})
.then(res => res.json())
.then(data => {

	if(data){

		window.location.replace("./courses.html");
	} else {

		alert("something went wrong");

	}
})

		



