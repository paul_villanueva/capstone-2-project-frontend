//get the courses
let token = localStorage.getItem("token");

let adminUser = localStorage.getItem("isAdmin");
let cardFooter;
let modalButton = document.querySelector("#adminButton");

if(adminUser == "false" || !adminUser) {

	modalButton.innerHTML = null;

} else {

	modalButton.innerHTML =
	`
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-primary">
				Add Course
			</a>
		</div>
	`

}

fetch('https://pure-ravine-04682.herokuapp.com/api/courses')
.then(res => res.json())
.then(data => {

	console.log(data);

	// Creates a variable that will store the data to be rendered
	let courseData = data;

	if(data.length < 1) {

		courseData = "No courses available"

	} else {

		courseData = data.map(course => {
			console.log(course);
			//if regular user
			if(adminUser == "false" || !adminUser) {

				if(course.isActive === true) {

				cardFooter =
					`
						<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn btn-primary text-white btn-block editButton">
							Select Course
						</a>
					`
					return (
						`
							<div class="col-md-6 my-3">
								<div class="card">
									<div class="card-body">
										<h5 class="card-title">
											${course.name}
										</h5>
										<p class="card-text text-left">
											${course.description}
										</p>
										<p class="card-text text-right">
											₱ ${course.price}
										</p>
									</div>
									<div class="card-footer">
										${cardFooter}
									</div>
								</div>
							</div>
						`
					)
				}

			//if admin user
			} else {
				if(course.isActive === true) {

					cardFooter =
					`
						<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn btn-info text-white btn-block editButton">
							View Course
						</a>
						<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-primary text-white btn-block editButton">
							Edit
						</a>
						<a href="./deleteCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-danger text-white btn-block deleteButton">
							Disable Course 
						</a>
					`					
				} else {

					cardFooter =
					`
						<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn btn-info text-white btn-block editButton">
							View Course
						</a>
						<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-primary text-white btn-block editButton">
							Edit
						</a>
						<a href="./undeleteCourse.html?courseId=${course._id}" value="${course._id}" class="btn btn-success text-white btn-block deleteButton">
							Enable Course
						</a>
					`

				}

				return (
					`
						<div class="col-md-6 my-3">
							<div class="card">
								<div class="card-body">
									<h5 class="card-title">
										${course.name}
									</h5>
									<p class="card-text text-left">
										${course.description}
									</p>
									<p class="card-text text-right">
										₱ ${course.price}
									</p>
								</div>
								<div class="card-footer">
									${cardFooter}
								</div>
							</div>
						</div>
					`
				)

			}


		// Replaces the comma's in the array with an empty string
		}).join("");

		//1st iteration
		/*course = {
			createdOn: "2021-03-02T09:19:40.227Z"
			description: "Let's make our website look cool!"
			enrollees: []
			isActive: true
			name: "CSS"
			price: 500
		}*/

		// 2nd iteration
		/*course = {
			createdOn: "2021-03-02T09:19:40.227Z"
			description: "A basic programming language."
			enrollees: []
			isActive: true
			name: "HTML"
			price: 1000
		}*/

	}

	document.querySelector("#coursesContainer").innerHTML = courseData;

})

// courseData = [

// 	`
// 	<div class="col-md-6 my-3">
// 		<div class="card">
// 			<div class="card-body">
// 				<h5 class="card-title">
// 					CSS
// 				</h5>
// 				<p class="card-text text-left">
// 					Let's make our websites look cooler
// 				</p>
// 				<p class="card-text text-right">
// 					₱ 500
// 				</p>
// 			</div>
// 		</div>
// 	</div>
// 	<div class="col-md-6 my-3">
// 		<div class="card">
// 			<div class="card-body">
// 				<h5 class="card-title">
// 					HTML
// 				</h5>
// 				<p class="card-text text-left">
// 					A basic programming language
// 				</p>
// 				<p class="card-text text-right">
// 					₱ 1000
// 				</p>
// 			</div>
// 		</div>
// 	</div>
// 	`

// ]